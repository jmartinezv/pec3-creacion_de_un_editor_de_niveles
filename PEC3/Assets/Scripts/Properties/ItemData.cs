﻿using System;

[Serializable]
public class ItemData
{
    public int[] location;
    public string itemType;
}
