﻿using System;
using UnityEngine;

[Serializable]
public class LevelData 
{
    public string levelName;
    public int rows;
    public int columns;
    public int minX;
    public int minZ;
    public Vector2[] emptyFloor;
    public ItemData[] buildItems;
}
