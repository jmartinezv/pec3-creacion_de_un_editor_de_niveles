﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SaveManager : MonoBehaviour
{
    public Text levelName;

    private Vector2[] emptyFloor;
    private int columns, rows;
    private ItemData[] builtItems;
    private EditorManager editor;
    private string saveFolder;
    private PersistentData data;
    private float minX = 0;
    private float maxX = 0;
    private float minZ = 0;
    private float maxZ = 0;

    private void Start()
    {
        data = GameObject.Find("DataManager").GetComponent<PersistentData>();
        editor = GetComponent<EditorManager>();
        saveFolder = Application.dataPath + "/Levels/";
    }

    public void SaveLevel()
    {
        if (levelName.text == string.Empty)
            return;

        GetGridData();
        emptyFloor = GetEmptyFloor(columns, rows);


        builtItems = new ItemData[editor.builtObjects.Count];
        int index = 0;

        foreach (GameObject item in editor.builtObjects)
        {
            ItemData itemData = new ItemData();
            Vector3 pos = item.transform.position;

            itemData.itemType = item.tag;
            itemData.location = new int[] { Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.z) };

            builtItems[index] = itemData;
            index++;
        }

        LevelData level = new LevelData();
        level.levelName = levelName.text;
        level.columns = columns;
        level.rows = rows;
        level.buildItems = builtItems;
        level.emptyFloor = emptyFloor;
        level.minX = Mathf.RoundToInt(minX);
        level.minZ = Mathf.RoundToInt(minZ);
        Debug.Log(level.levelName);
        data.AddLevel(level.levelName);

        string json = JsonUtility.ToJson(level);
        Debug.Log(json);
        if (!Directory.Exists(saveFolder))
            Directory.CreateDirectory(saveFolder);

        File.WriteAllText(saveFolder + level.levelName +".txt", json);
    }

    private void GetGridData()
    {
        GameObject[] floorTiles = GameObject.FindGameObjectsWithTag("Floor");

        for (int i = 0; i < floorTiles.Length; i++)
        {
            Vector3 pos = floorTiles[i].transform.position;

            if (pos.x < minX)
                minX = pos.x;
            if (pos.x > maxX)
                maxX = pos.x;
            if (pos.z < minZ)
                minZ = pos.z;
            if (pos.z > maxZ)
                maxZ = pos.z;
        }
        //Sumamos 1 por la posición 0 (2-0 = 2, pero son 3 columnas/filas)
        columns = Mathf.RoundToInt(maxX - minX + 1);
        rows = Mathf.RoundToInt(maxZ - minZ + 1);
        Debug.Log("Columns: " + columns + ", Rows: " + rows);
    }
    
    private Vector2[] GetEmptyFloor(int x, int y)
    {
        List<Vector2> emptyFloorList = new List<Vector2>();

        for (int i = Mathf.RoundToInt(minX); i < x; i++)
        {
            for (int j = Mathf.RoundToInt(minZ); j < y; j++)
            {
                if (editor.CheckFloor(new Vector3(i, 0, j)) == null)
                {
                    Debug.Log("IS EMPTY: " + i + ", " + j);
                    emptyFloorList.Add(new Vector2(i, j));
                }
            }
        }
        return emptyFloorList.ToArray();
    }

}
