﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PersistentData : MonoBehaviour
{
    public List<string> levelList = new List<string>();
    public string loadLevel = "";

    private LevelNames levelNames;
    private string json;
    private string newName;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(transform.gameObject);
        levelList.Clear();
        GenerateNameList();
    }

    public void GenerateNameList()
    {
        json = File.ReadAllText(Application.dataPath + "/Data/LevelNames.txt");
        levelNames = JsonUtility.FromJson<LevelNames>(json);

        if (levelNames == null)
            return;

        foreach (string name in levelNames.names)
        {
            levelList.Add(name);
        }
    }

    public void RemoveLevel(string name)
    {
        levelList.Remove(name);
        WriteToJson();
    }

    public void AddLevel(string name)
    {
        newName = name;
        if (!levelList.Exists(CheckName))
            levelList.Add(name);

        WriteToJson();
    }

    private bool CheckName(string name)
    {
        return name == newName;
    }

    private void WriteToJson()
    {
        //LevelNames newLevels = new LevelNames();
        levelNames.names = levelList.ToArray();
        json = JsonUtility.ToJson(levelNames);
        Debug.Log(json);
        File.WriteAllText(Application.dataPath + "/Data/LevelNames.txt", json);
        GenerateNameList();
    }
}
