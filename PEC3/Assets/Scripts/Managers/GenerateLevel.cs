﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GenerateLevel : MonoBehaviour
{
    public GameObject floorPrefab;
    public GameObject wallPrefab;
    public GameObject playerPrefab;
    public GameObject boxPrefab;
    public GameObject goalPrefab;

    private LevelData level;
    private List<Vector2> emptyTiles = new List<Vector2>();
    private Vector2 emptyPosition = Vector2.zero;
    private PersistentData data;

    [HideInInspector]
    public List<GameObject> builtItems = new List<GameObject>();

    private void Start()
    {
        data = GameObject.Find("DataManager").GetComponent<PersistentData>();
        LoadLevel(data.loadLevel, true);
    }

    public void LoadLevel(string levelName, bool withWalls)
    {
        emptyTiles.Clear();
        string json = File.ReadAllText(Application.dataPath + "/Levels/"+ levelName +".txt");
        Debug.Log(json);

        level = JsonUtility.FromJson<LevelData>(json);

        for (int i = 0; i < level.emptyFloor.Length; i++)
        {
            emptyTiles.Add(level.emptyFloor[i]);
        }


        //Genera las tiles del suelo y los muros alrededor
        GenerateMapTiles(withWalls);
        //Genera las estructuras
        GenerateBuiltItems();
    }

    private bool isEmpty(Vector2 position)
    {
        return (emptyPosition == position);
    }

    private void GenerateMapTiles(bool withWalls)
    {
        //Creamos dos items vacíos para organizarnos en el editor:
        GameObject floorParent = GameObject.Find("floorParent");
        GameObject wallParent = GameObject.Find("wallParent");
        int minColumn = level.minX;
        int minRow = level.minZ;
        int maxColumn = level.columns + level.minX;
        int maxRow = level.rows + level.minZ;

        for (int i = minColumn; i < maxColumn; i++)
        {
            for (int j = minRow; j < maxRow; j++)
            {
                GameObject prefab = null;

                emptyPosition = new Vector2(i, j);
                //Ponemos muros en los huecos 
                if (withWalls)
                {
                    if (!emptyTiles.Exists(isEmpty))
                    {
                        Vector3 position = new Vector3(i, -1, j);
                        prefab = Instantiate(floorPrefab, position, Quaternion.identity);
                        prefab.name = "Floor (" + i + ", " + j + ")";
                        prefab.transform.parent = floorParent.transform;


                        //Ponemos los muros externos cuando estamos en la primera y última fila y columna.
                        if (i == minColumn || i == maxColumn - 1)
                        {
                            if (i == minColumn)
                                prefab = Instantiate(wallPrefab, new Vector3(minColumn - 1, 0, j), Quaternion.identity);
                            else if (i == maxColumn - 1)
                                prefab = Instantiate(wallPrefab, new Vector3(maxColumn, 0, j), Quaternion.identity);

                            prefab.name = "Wall (" + i + ", " + j + ")";
                            prefab.transform.parent = wallParent.transform;
                        }

                        if (j == minRow || j == maxRow - 1)
                        {
                            if (j == minRow)
                                prefab = Instantiate(wallPrefab, new Vector3(i, 0, minRow - 1), Quaternion.identity);
                            else if (j == maxRow - 1)
                                prefab = Instantiate(wallPrefab, new Vector3(i, 0, maxRow), Quaternion.identity);
                            prefab.name = "Wall (" + i + ", " + j + ")";
                            prefab.transform.parent = wallParent.transform;
                        }
                    }
                    else
                    {
                        Debug.Log("x: " + i + " y: " + j);
                        prefab = Instantiate(wallPrefab, new Vector3(i, 0, j), Quaternion.identity);
                        prefab.name = "Wall (" + i + ", " + j + ")";
                        prefab.transform.parent = wallParent.transform;
                    }
                }
                else
                {
                    if (!emptyTiles.Exists(isEmpty))
                    {
                        Vector3 position = new Vector3(i, -1, j);
                        prefab = Instantiate(floorPrefab, position, Quaternion.identity);
                        prefab.name = "Floor (" + i + ", " + j + ")";
                        prefab.transform.parent = floorParent.transform;
                    }
                }
            }
        }
    }

    private void GenerateBuiltItems()
    {
        GameObject levelGeneration = GameObject.Find("levelGeneration");
        if ( levelGeneration == null)
        {
            levelGeneration = new GameObject();
            levelGeneration.name = "levelGeneration";
        }

        for (int i = 0; i < level.buildItems.Length; i++)
        {
            ItemData item = level.buildItems[i];
            Vector3 position = new Vector3(item.location[0], 0, item.location[1]);

            GameObject built = null;

            if (item.itemType == "Box")
                built = Instantiate(boxPrefab, position, Quaternion.identity);
            else if (item.itemType == "Goal")
                built = Instantiate(goalPrefab, position, Quaternion.identity);
            else if (item.itemType == "Wall")
                built = Instantiate(wallPrefab, position, Quaternion.identity);
            else if (item.itemType == "Player")
                built = Instantiate(playerPrefab, position, Quaternion.identity);


            builtItems.Add(built);
            built.transform.parent = levelGeneration.transform;
        }
    }
    
}
