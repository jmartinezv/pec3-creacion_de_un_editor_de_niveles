﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public GameObject levelPrefab;
    public GameObject levelPanel;
    public GameObject buttonPanel;
    public GameObject newLevelButton;
    public float buttonSpace = 200f;

    private PersistentData data;
    private List<GameObject> buttons = new List<GameObject>();

    private void Start()
    {
        data = GameObject.Find("DataManager").GetComponent<PersistentData>();
    }
    

    public void LoadLevel(string name)
    {
        data.loadLevel = name;
        SceneManager.LoadScene("GamePlay");
    }

    public void EditLevel(string name)
    {
        data.loadLevel = name;
        SceneManager.LoadScene("Editor");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void PanelEdit()
    {
        levelPanel.SetActive(true);
        GenerateLevelButtons(true);

        newLevelButton.SetActive(true);
    }

    public void PlayLevels()
    {
        levelPanel.SetActive(true);
        GenerateLevelButtons(false);

        newLevelButton.SetActive(false);
    }
    
    public void Back()
    {
        levelPanel.SetActive(false);
        foreach (GameObject button in buttons)
        {
            Destroy(button);
        }
        buttons.Clear();
    }

    private void GenerateLevelButtons(bool edit)
    {
        Vector3 butPos = Vector3.zero;
        int index = 0;
        string json = File.ReadAllText(Application.dataPath + "/Data/LevelNames.txt");

        foreach (string name in data.levelList)
        {
            GameObject newButton = Instantiate(levelPrefab, buttonPanel.transform, false);
            if (index == 0)
            {
                butPos = newButton.GetComponent<RectTransform>().position;
            }
            else
            {
                butPos.x += buttonSpace;
                newButton.GetComponent<RectTransform>().position = butPos;
            }
            newButton.GetComponentInChildren<Text>().text = name;

            if (edit)
                newButton.GetComponent<Button>().onClick.AddListener(delegate { EditLevel(name); });
            else
                newButton.GetComponent<Button>().onClick.AddListener(delegate { LoadLevel(name); });

            buttons.Add(newButton);
            index++;
        }
    }
}

public class LevelNames
{
    public string[] names;
}
