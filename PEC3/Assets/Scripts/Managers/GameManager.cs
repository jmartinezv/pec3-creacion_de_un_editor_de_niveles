﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text goalsUI;
    public Text levelName;
    public float transtitionTime = 1f;

    private static int remainingGoals;
    private static int boxOnGoal = 0;
    private PersistentData data;
    private bool dataLoaded = false;
    private bool endgame = false;
    private float timer = 0f;
    private GameObject[] boxes;

    // Start is called before the first frame update
    void Start()
    {
        data = GameObject.Find("DataManager").GetComponent<PersistentData>();
        levelName.text = data.loadLevel;
    }

    // Update is called once per frame
    void Update()
    {
        if (endgame)
            return;

        if (!dataLoaded)
            LoadData();

        CheckScore();

        if (boxOnGoal == remainingGoals)
        {
            timer += Time.deltaTime;
            if (timer >= transtitionTime)
            {
                timer = 0;
                NextLevel();
            }
        }
    }

    private void CheckScore()
    {
        int score = 0;
        foreach (GameObject box in boxes)
        {
            if (box.GetComponent<Box>().onGoal)
            {
                score++;
            }
        }
        boxOnGoal = score;

        goalsUI.text = boxOnGoal.ToString() + "/" + remainingGoals.ToString();
    }

    private void LoadData()
    {
        remainingGoals = GameObject.FindGameObjectsWithTag("Goal").Length;
        boxes = GameObject.FindGameObjectsWithTag("Box");
    }
    
    private void NextLevel()
    {
        endgame = true;
        boxOnGoal = 0;
        DestroyLevel();

        int index = 0;
        foreach (string name in data.levelList)
        {
            if (name == data.loadLevel)
                break;
            index++;
        }
        if (index >= data.levelList.Count)
        {
            endgame = false;
            SceneManager.LoadScene("Menú");
        }

        string nextLevel = data.levelList[index + 1];
        data.loadLevel = nextLevel;
        levelName.text = data.loadLevel;

        StartCoroutine(LoadLevel(nextLevel));
    }

    IEnumerator LoadLevel(string nextLevel)
    {
        yield return new WaitForSeconds(1f);
        gameObject.GetComponent<GenerateLevel>().LoadLevel(nextLevel, true);
        endgame = false;
    }

    private void DestroyLevel()
    {
        GameObject level = GameObject.Find("levelGeneration");

        Destroy(level.gameObject);

        GameObject newLevel = new GameObject();
        newLevel.name = "levelGeneration";
        GameObject wallParent = new GameObject();
        GameObject floorParent = new GameObject();
        wallParent.name = "wallParent";
        floorParent.name = "floorParent";
        wallParent.transform.parent = newLevel.transform;
        floorParent.transform.parent = newLevel.transform;
    }

    public void Back()
    {
        SceneManager.LoadScene("Menú");
    }

    public void Restart()
    {
        DestroyLevel();
        LoadLevel(data.loadLevel);
    }
}
