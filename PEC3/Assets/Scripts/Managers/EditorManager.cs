﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EditorManager : MonoBehaviour
{
    public enum SelTool {None, Erase, Floor, Wall, Box, Goal, Player};

    public GameObject cursor;
    public GameObject[] selectedObject;
    public GameObject floorPrefab;
    public GameObject wallPrefab;
    public GameObject playerPrefab;
    public GameObject boxPrefab;
    public GameObject goalPrefab;
    public SelTool selectedTool;

    private bool elevated;
    private PersistentData data;
    private GenerateLevel gen = new GenerateLevel();
    private bool moving;

    [HideInInspector]
    public List<GameObject> builtObjects = new List<GameObject>();


    private void Start()
    {
        data = GameObject.Find("DataManager").GetComponent<PersistentData>();

        if (data.loadLevel == string.Empty)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    GameObject builtItem = Instantiate(floorPrefab, new Vector3(i, -1, j), Quaternion.identity);
                }
            }
        }
        else
        {
            gen.boxPrefab = boxPrefab;
            gen.floorPrefab = floorPrefab;
            gen.wallPrefab = wallPrefab;
            gen.playerPrefab = playerPrefab;
            gen.goalPrefab = goalPrefab;
            gen.LoadLevel(data.loadLevel, false);

            if (GameObject.FindGameObjectWithTag("Player") != null)
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().inEditor = true;
            }
            foreach (GameObject item in gen.builtItems)
            {
                builtObjects.Add(item);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!moving) 
            CursorPosition();

        CheckCursorElevation();

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (selectedTool != SelTool.None && selectedTool != SelTool.Erase)
                BuildItem();
            else if (selectedTool == SelTool.Erase)
                DeleteItem();
        }

            
    }

    private GameObject TileOccupied(Vector3 location)
    {
        Collider[] hitColliders;
        if (elevated)
            hitColliders = Physics.OverlapSphere(location, 0.5f);
        else
            hitColliders = Physics.OverlapSphere(location, 0.25f);

        if (hitColliders.Length > 0)
            return hitColliders[0].gameObject;
        else
            return null;

    }

    public GameObject CheckFloor(Vector3 location)
    {
        RaycastHit hit;

        if (Physics.Raycast(location, Vector3.down, out hit, Mathf.Infinity))
            return hit.collider.gameObject;

        return null;
    
    }

    private void CursorPosition()
    {
        int horizontal = (int)(Input.GetAxisRaw("Horizontal"));
        int vertical = (int)(Input.GetAxisRaw("Vertical"));

        if (horizontal != 0)
            vertical = 0;

        if (vertical > 0)
            cursor.transform.position += Vector3.forward;
        else if (vertical < 0)
            cursor.transform.position += Vector3.back;
        else if (horizontal > 0)
            cursor.transform.position += Vector3.right;
        else if (horizontal < 0)
            cursor.transform.position += Vector3.left;
        
        StartCoroutine(SmoothMovement());
    }

    protected IEnumerator SmoothMovement()
    {
        moving = true;
        yield return new WaitForSeconds(.1f);
        moving = false;
    }



    public void ResetCursorPosition()
    {
        cursor.transform.position = Vector3.zero;
    }

    private void CheckCursorElevation()
    {
        Vector3 spawnPoint = cursor.transform.position;

        if (TileOccupied(spawnPoint))
        {
            spawnPoint.y = 1;
            elevated = true;
        }
        else
        {
            spawnPoint.y = 0;
            elevated = false;
        }

        cursor.transform.position = spawnPoint;
    }

    private void BuildItem()
    {
        //Si la casilla ya está ocupada, no hacemos nada (lo suyo sería dar algún tipo de feedback auditivo o visual)
        if (TileOccupied(cursor.transform.position) != null)
            return;
        if ((CheckFloor(cursor.transform.position) == null  && selectedTool != SelTool.Floor) || (CheckFloor(cursor.transform.position) != null && selectedTool == SelTool.Floor))
            return;

        GameObject builtParent = GameObject.Find("BuiltItems");
        GameObject builtItem;

        switch (selectedTool)
        {
            case SelTool.Floor:
                builtItem = Instantiate(floorPrefab, cursor.transform.position + Vector3.down, Quaternion.identity);
                builtItem.transform.parent = builtParent.transform;
                break;
            case SelTool.Wall:
                builtItem = Instantiate(wallPrefab, cursor.transform.position, Quaternion.identity);
                builtItem.transform.parent = builtParent.transform;
                builtObjects.Add(builtItem);
                break;
            case SelTool.Box:
                builtItem = Instantiate(boxPrefab, cursor.transform.position, Quaternion.identity);
                builtItem.transform.parent = builtParent.transform;
                builtObjects.Add(builtItem);
                break;
            case SelTool.Goal:
                builtItem = Instantiate(goalPrefab, cursor.transform.position, Quaternion.identity);
                builtItem.transform.parent = builtParent.transform;
                builtObjects.Add(builtItem);
                break;
            case SelTool.Player:
                if (GameObject.FindGameObjectWithTag("Player") != null)
                {
                    builtObjects.Remove(GameObject.FindGameObjectWithTag("Player"));
                    Destroy(GameObject.FindGameObjectWithTag("Player"));
                }
                builtItem = Instantiate(playerPrefab, cursor.transform.position, Quaternion.identity);
                builtItem.transform.parent = builtParent.transform;
                builtObjects.Add(builtItem);
                builtItem.GetComponent<Player>().inEditor = true;
                break;
            default:
                break;
        }
    }

    private void DeleteItem()
    {
        GameObject floor = CheckFloor(cursor.transform.position);
        GameObject item = TileOccupied(cursor.transform.position);

        if (item != null)
        {
            Destroy(item);
            builtObjects.Remove(item);
        }
        else if (floor != null)
            Destroy(floor);

    }

    public void SelectTool(string selTool)
    {
        SelTool selButton = SelTool.None;
        for (int i = 0; i < selectedObject.Length; i++)
        {
            selectedObject[i].SetActive(false);
        }

        if (selTool == "Erase" && selectedTool != SelTool.Erase)
        {
            selButton = SelTool.Erase;
            selectedObject[0].SetActive(true);
        }
        else if (selTool == "Floor" && selectedTool != SelTool.Floor)
        {
            selButton = SelTool.Floor;
            selectedObject[1].SetActive(true);
        }
        else if (selTool == "Wall" && selectedTool != SelTool.Wall)
        {
            selButton = SelTool.Wall;
            selectedObject[2].SetActive(true);
        }
        else if (selTool == "Box" && selectedTool != SelTool.Box)
        {
            selButton = SelTool.Box;
            selectedObject[3].SetActive(true);
        }
        else if (selTool == "Goal" && selectedTool != SelTool.Goal)
        {
            selButton = SelTool.Goal;
            selectedObject[4].SetActive(true);
        }
        else if (selTool == "Player" && selectedTool != SelTool.Player)
        {
            selButton = SelTool.Player;
            selectedObject[5].SetActive(true);
        }

        selectedTool = selButton;
    }

    public void ExitEditor()
    {
        SceneManager.LoadScene("Menú");
    }

    
    

}
