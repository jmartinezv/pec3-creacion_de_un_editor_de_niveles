﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : MonoBehaviour
{
    public float moveTime = 0.5f;            
    public LayerMask blockingLayer;
    public LayerMask pushingLayer;

    protected BoxCollider boxCollider;         
    protected Rigidbody rb;    
    protected bool moving;
    private float inverseMoveTime;           //Se utiliza por temas de eficiencia (no realmente necesario en este caso)



    protected virtual void Start()
    {

        boxCollider = GetComponent<BoxCollider>();
        rb = GetComponent<Rigidbody>();

        //Usando la inversa del tiempo de movimiento, los calculos serán multiplicaciones en vez de divisiones, siendo más eficiente.
        inverseMoveTime = 1f / moveTime;
    }

    //Devuelve TRUE si puede moverse, o FALSE si no
    public bool Move(int xDir, int zDir, out RaycastHit hit)
    {
        Vector3 start = transform.position;
        Vector3 end = start + new Vector3(xDir,0,zDir);

        boxCollider.enabled = false;
        Physics.Linecast(start, end, out hit, blockingLayer,QueryTriggerInteraction.Ignore);
        boxCollider.enabled = true;

        if (hit.transform == null)
        {
            moving = true;

            StartCoroutine(SmoothMovement(end));
            return true;
        }

        return false;
    }

    public bool Push(int xDir, int zDir, out RaycastHit hit)
    {
        Vector3 start = transform.position;
        Vector3 end = start + new Vector3(xDir, 0, zDir);
        boxCollider.enabled = false;
        Physics.Linecast(start, end, out hit, pushingLayer, QueryTriggerInteraction.Ignore);
        boxCollider.enabled = true;

        if (hit.transform == null)
            return Move(xDir, zDir, out hit);

        if (hit.transform.GetComponent<Box>().Push(xDir, zDir, out hit))
            return Move(xDir, zDir, out hit);

        return false;
    }


    protected IEnumerator SmoothMovement(Vector3 end)
    {
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;

        while (sqrRemainingDistance > 0.001f)
        {
            Vector3 newPostion = Vector3.MoveTowards(rb.position, end, inverseMoveTime * Time.deltaTime);

            rb.MovePosition(newPostion);

            //Recalculate the remaining distance after moving.
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;

            yield return null;
        }

        MoveEnded();
    }

    protected virtual void MoveEnded()
    {
        moving = false;
    }

    
}