﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MovingObject
{

    public bool onGoal = false;
    private GameObject goal;
    /*
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Goal")
            return;

        if (onGoal && goal == other.gameObject)
            return;


        onGoal = true;
        goal = other.gameObject;
        //GameManager.OnGoal(onGoal);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag != "Goal")
            return;

        onGoal = false;
        //GameManager.OnGoal(onGoal);
    }
    */
    private void Update()
    {
        CheckOnGoal();
    }

    private void CheckOnGoal()
    {
        bool checkGoal = false;
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 0.25f);
        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].gameObject.tag == "Goal")
                checkGoal = true;
        }
        onGoal = checkGoal;
    }

    // Start is called before the first frame update
    protected override void MoveEnded()
    {
        base.MoveEnded();
    }
    
}
